<?php 
    include "connection.php";
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Movie DB</title>
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>

  <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal">Movie DB</h5>
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark" href="index.php">Home</a>
  </nav>
  <a class="btn btn-outline-primary" href="setting.php">Setting</a>
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Edit Movie</h1>
  </div>

  <?php

    $query = $connect->query("SELECT * FROM Movie WHERE id = '$_GET[id]'");
    $row = $query->fetch_assoc();
    ?>
<div class="container">
<form method="post" action="_edit.php">
    <input type="hidden" name="id" value="<?php echo $row['id']?>">
  <div class="form-group">
    <label for="titleMovie">Movie Title</label>
    <input type="text" class="form-control" id="titleMovie" aria-describedby="titleMovie" placeholder="Title Movie" name="movie" value="<?php echo $row['movie']?>">
  </div>
  <div class="form-group">
    <label for="genre">Genre</label>
    <input type="text" class="form-control" id="genre" placeholder="Movie Genre" name="genre" value="<?php echo $row['genre']?>">
  </div>
  <div class="form-group">
  <label for="genre">Production House</label>
    <select class="custom-select" name="prodHouse">

  <?php

    $query = $connect->query("SELECT * FROM ProductionHouse");

    if($query->num_rows > 0) {
        while($row = $query->fetch_assoc()) {
            if($_GET['id'] == $row['idProd']) {
                echo "<option value='".$row['idProd']."' selected>".$row['name']."</option>";
            }else {
                echo "<option value='".$row['idProd']."'>".$row['name']."</option>";
            }
            
        }
    }
    ?>
    </select>   
  </div>
  
  <input type="submit" class="btn btn-primary" value="Add Movie"></input>
</form>


  <footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
      <div class="col-12 col-md">
        <img class="mb-2" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
        <small class="d-block mb-3 text-muted">&copy; Hacktiv8 2019</small>
      </div>
      <div class="col-6 col-md">
        <h5>The Basic</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">About Movie DB</a></li>
          <li><a class="text-muted" href="#">Blog</a></li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5>Legal</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">Terms of Use</a></li>
          <li><a class="text-muted" href="#">Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5>Creator</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">Ziady Mubaraq</a></li>
        </ul>
      </div>
    </div>
  </footer>
</div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>